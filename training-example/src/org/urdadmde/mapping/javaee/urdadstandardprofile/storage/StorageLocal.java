package org.urdadmde.mapping.javaee.urdadstandardprofile.storage;

import javax.ejb.Local;

@Local
public interface StorageLocal extends Storage{}