package org.urdadmde.mapping.javaee.urdadstandardprofile.storage;

import javax.ejb.Remote;

@Remote
public interface StorageRemote extends Storage{}