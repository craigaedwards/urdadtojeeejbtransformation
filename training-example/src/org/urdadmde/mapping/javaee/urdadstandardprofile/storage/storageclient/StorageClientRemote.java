package org.urdadmde.mapping.javaee.urdadstandardprofile.storage.storageclient;

import javax.ejb.Remote;

@Remote
public interface StorageClientRemote extends StorageClient{}