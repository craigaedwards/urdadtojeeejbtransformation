package org.urdadmde.mapping.javaee.urdadstandardprofile.storage;

import java.util.concurrent.Future;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class StorageBean implements StorageLocal, StorageRemote
{

	@Override
	public StoreEntityResult storeEntity(StoreEntityRequest storeEntityRequest)
			throws EntityAlreadyStoredException
	{
		return null;
	}
	
	@Override
	@Asynchronous
	public Future<StoreEntityResult> storeEntityAsynchronously(StoreEntityRequest storeEntityRequest) throws
		EntityAlreadyStoredException
	{
		return new AsyncResult<StoreEntityResult>(storeEntity(storeEntityRequest));
	}

	@Override
	public RetrieveEntityResult retrieveEntity(RetrieveEntityRequest retrieveEntityRequest) throws
		EntityNotStoredException
	{
		/* Example of entity identifier is 'org/amazon/product/ereader/Kindle.1' */
		String primaryKey = retrieveEntityRequest.getEntityIdentifier().substring(retrieveEntityRequest
			.getEntityIdentifier().indexOf('.') + 1);
		String entityClass = retrieveEntityRequest.getEntityIdentifier().substring(0, retrieveEntityRequest
			.getEntityIdentifier().indexOf('.'));
		entityClass = entityClass.replace('/', '.');
		RetrieveEntityResult retrieveEntityResult = new RetrieveEntityResult();
		try{
			retrieveEntityResult.setEntity((Entity) entityManager.find(Class.forName(entityClass), 
				primaryKey));
		}
		catch (ClassNotFoundException e){
			throw new EntityNotStoredException();
		}
		catch (IllegalArgumentException e){
			throw new EntityNotStoredException();
		}
		return retrieveEntityResult;
	}
	
	@Override
	@Asynchronous	
	public Future<RetrieveEntityResult> retrieveEntityAsynchronously(RetrieveEntityRequest 
		retrieveEntityRequest) throws EntityNotStoredException
	{
		return new AsyncResult<RetrieveEntityResult>(retrieveEntity(retrieveEntityRequest));
	}

	@Override
	public RemoveEntityResult removeEntity(RemoveEntityRequest removeEntityRequest) throws 
		EntityNotStoredException
	{
		return null;
	}
	
	@Override
	@Asynchronous
	public Future<RemoveEntityResult> removeEntityAsynchronously(RemoveEntityRequest removeEntityRequest) 
		throws EntityNotStoredException
	{
		return new AsyncResult<RemoveEntityResult>(removeEntity(removeEntityRequest));
	}

	@PersistenceContext()
	protected EntityManager entityManager;

}