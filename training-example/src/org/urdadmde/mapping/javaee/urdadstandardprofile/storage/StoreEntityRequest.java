package org.urdadmde.mapping.javaee.urdadstandardprofile.storage;

import java.io.Serializable;

public class StoreEntityRequest implements Serializable{

	public Entity getEntity()
	{
		return (Entity) entity;
	}

	public void setEntity(Entity entity)
	{
		this.entity = (Entity) entity;
	}

	private Entity entity;

}