package org.urdadmde.mapping.javaee.urdadstandardprofile.storage;

import java.util.concurrent.Future;

public interface Storage
{

	public StoreEntityResult storeEntity(StoreEntityRequest storeEntityRequest) throws
		EntityAlreadyStoredException;

	public Future<StoreEntityResult> storeEntityAsynchronously(StoreEntityRequest storeEntityRequest) throws
		EntityAlreadyStoredException;
	
	public RetrieveEntityResult retrieveEntity(RetrieveEntityRequest retrieveEntityRequest) throws
		EntityNotStoredException;
	
	public Future<RetrieveEntityResult> retrieveEntityAsynchronously(RetrieveEntityRequest 
		retrieveEntityRequest) throws EntityNotStoredException;
	
	public RemoveEntityResult removeEntity(RemoveEntityRequest removeEntityRequest) throws 
		EntityNotStoredException;

	public Future<RemoveEntityResult> removeEntityAsynchronously(RemoveEntityRequest removeEntityRequest) 
		throws EntityNotStoredException;
	
}