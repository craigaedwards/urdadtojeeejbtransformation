package org.urdadmde.mapping.javaee.urdadstandardprofile.storage.storageclient;

import javax.ejb.Local;

@Local
public interface StorageClientLocal extends StorageClient{}