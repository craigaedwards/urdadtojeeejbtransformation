package org.urdadmde.mapping.javaee.urdadstandardprofile.storage.storageclient;

import javax.ejb.Stateless;

@Stateless
public class StorageClientBean implements StorageClientLocal, StorageClientRemote{}