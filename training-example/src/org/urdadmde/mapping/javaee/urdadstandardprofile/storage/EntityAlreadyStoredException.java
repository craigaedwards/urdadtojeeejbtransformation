package org.urdadmde.mapping.javaee.urdadstandardprofile.storage;

public class EntityAlreadyStoredException extends Exception{}