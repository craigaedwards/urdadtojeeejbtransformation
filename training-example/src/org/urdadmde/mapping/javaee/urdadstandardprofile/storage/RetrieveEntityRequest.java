package org.urdadmde.mapping.javaee.urdadstandardprofile.storage;

import java.io.Serializable;

public class RetrieveEntityRequest implements Serializable{

	public String getEntityIdentifier()
	{
		return entityIdentifier;
	}

	public void setEntityIdentifier(String entityIdentifier)
	{
		this.entityIdentifier = entityIdentifier;
	}

	private String entityIdentifier;

}