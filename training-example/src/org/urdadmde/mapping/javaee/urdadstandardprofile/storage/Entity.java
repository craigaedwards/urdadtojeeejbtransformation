package org.urdadmde.mapping.javaee.urdadstandardprofile.storage;

import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Entity implements Serializable{

	public String getEntityIdentifier(){
		return entityIdentifier;
	}

	public void setEntityIdentifier(String entityIdentifier){
		this.entityIdentifier = entityIdentifier;
	}

	@Id
	private String entityIdentifier;

}