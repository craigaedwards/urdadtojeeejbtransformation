package org.urdadmde.mapping.javaee.urdadstandardprofile.storage;

public class EntityNotStoredException extends Exception{}