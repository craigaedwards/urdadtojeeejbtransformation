package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.coursemanagement;

import javax.ejb.Remote;

@Remote
public interface CourseManagementRemote extends CourseManagement{}