package org.urdadmde.mapping.javaee.example.training.traininginstitution.enrollments.proofofenrollments;

import org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.presentationmanagement.Presentation;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement.Person;
import org.urdadmde.mapping.javaee.urdadstandardprofile.storage.Entity;

@javax.persistence.Entity
public class ProofOfEnrollment extends Entity
{

	public Person getStudentDetails()
	{
		return studentDetails;
	}

	public void setStudentDetails(Person studentDetails)
	{
		this.studentDetails = studentDetails;
	}

	public Presentation getPresentationDetails()
	{
		return presentationDetails;
	}

	public void setPresentationDetails(Presentation presentationDetails)
	{
		this.presentationDetails = presentationDetails;
	}

	private Person studentDetails;
	private Presentation presentationDetails;

}