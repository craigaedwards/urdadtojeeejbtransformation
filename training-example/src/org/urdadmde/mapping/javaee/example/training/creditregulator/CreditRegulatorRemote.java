package org.urdadmde.mapping.javaee.example.training.creditregulator;

import javax.ejb.Remote;

@Remote
public interface CreditRegulatorRemote extends CreditRegulator{}