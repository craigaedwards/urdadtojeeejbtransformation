package org.urdadmde.mapping.javaee.example.training.traininginstitution;

import javax.ejb.Local;

@Local
public interface TrainingInstitutionLocal extends TrainingInstitution{}