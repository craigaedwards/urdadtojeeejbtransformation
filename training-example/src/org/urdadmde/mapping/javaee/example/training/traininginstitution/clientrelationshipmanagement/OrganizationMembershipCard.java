package org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement;

@javax.persistence.Entity
public class OrganizationMembershipCard extends PersonIdentification
{

	public String getCardNumber()
	{
		return cardNumber;
	}

	public void setCardNumber(String cardNumber)
	{
		this.cardNumber = cardNumber;
	}

	private String cardNumber;

}