package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic;

import javax.ejb.Stateless;

@Stateless
public class AcademicBean implements AcademicLocal, AcademicRemote{}