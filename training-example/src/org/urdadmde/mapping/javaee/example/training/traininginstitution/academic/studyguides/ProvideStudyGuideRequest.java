package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.studyguides;

import java.io.Serializable;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.coursemanagement.Course;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement.Person;

public class ProvideStudyGuideRequest implements Serializable
{

	public String getCourseIdentifier()
	{
		return courseIdentifier;
	}

	public void setCourseIdentifier(String courseIdentifier)
	{
		this.courseIdentifier = courseIdentifier;
	}

	public String getStudentIdentifier()
	{
		return studentIdentifier;
	}

	public void setStudentIdentifier(String studentIdentifier)
	{
		this.studentIdentifier = studentIdentifier;
	}

	private String courseIdentifier;
	private String studentIdentifier;

}