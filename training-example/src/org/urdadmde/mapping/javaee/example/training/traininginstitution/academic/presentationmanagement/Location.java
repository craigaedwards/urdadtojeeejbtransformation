package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.presentationmanagement;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement.Address;
import org.urdadmde.mapping.javaee.urdadstandardprofile.storage.Entity;

@javax.persistence.Entity
public class Location extends Entity
{

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Address getAddress()
	{
		return address;
	}

	public void setAddress(Address address)
	{
		this.address = address;
	}

	private String name;
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "address")
	private Address address;

}