package org.urdadmde.mapping.javaee.example.training.shareholder;

import javax.ejb.Remote;

@Remote
public interface ShareholderRemote extends Shareholder{}