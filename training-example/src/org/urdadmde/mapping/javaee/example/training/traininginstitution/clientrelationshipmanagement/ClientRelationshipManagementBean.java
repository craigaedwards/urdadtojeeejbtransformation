package org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement;

import java.util.concurrent.Future;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.jboss.ejb3.annotation.IgnoreDependency;
import org.urdadmde.mapping.javaee.urdadstandardprofile.storage.EntityNotStoredException;
import org.urdadmde.mapping.javaee.urdadstandardprofile.storage.RetrieveEntityRequest;
import org.urdadmde.mapping.javaee.urdadstandardprofile.storage.RetrieveEntityResult;
import org.urdadmde.mapping.javaee.urdadstandardprofile.storage.StorageLocal;

@Stateless
public class ClientRelationshipManagementBean implements ClientRelationshipManagementLocal,
	ClientRelationshipManagementRemote
{

	@Override
	public ProvideRegistrationDetailsResult provideRegistrationDetails(ProvideRegistrationDetailsRequest
		provideRegistrationDetailsRequest) throws PersonNotRegisteredException
	{
		RetrieveEntityRequest retrieveEntityRequest = new RetrieveEntityRequest();
		retrieveEntityRequest.setEntityIdentifier(provideRegistrationDetailsRequest.getPersonIdentifier());
		RetrieveEntityResult resultEntityResult = null;
		try
		{
			resultEntityResult = storage.retrieveEntity(retrieveEntityRequest);
		}
		catch (EntityNotStoredException e)
		{
			throw new PersonNotRegisteredException();
		}
		ProvideRegistrationDetailsResult provideRegistrationDetailsResult = new
			ProvideRegistrationDetailsResult();
		provideRegistrationDetailsResult.setPersonDetails((Person) resultEntityResult.getEntity());
		return provideRegistrationDetailsResult;
	}
	
	@Override
	@Asynchronous
	public Future<ProvideRegistrationDetailsResult> provideRegistrationDetailsAsynchronously
		(ProvideRegistrationDetailsRequest provideRegistrationDetailsRequest) throws 
		PersonNotRegisteredException
	{
		return new AsyncResult<ProvideRegistrationDetailsResult>(provideRegistrationDetails
			(provideRegistrationDetailsRequest));
	}

	@EJB
	@IgnoreDependency
	private StorageLocal storage;

}