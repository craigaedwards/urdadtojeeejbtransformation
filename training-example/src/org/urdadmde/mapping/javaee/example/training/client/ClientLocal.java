package org.urdadmde.mapping.javaee.example.training.client;

import javax.ejb.Local;

@Local
public interface ClientLocal extends Client{}