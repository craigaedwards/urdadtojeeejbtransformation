package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment.validation;

import javax.ejb.Local;

@Local
public interface ValidationLocal extends Validation{}