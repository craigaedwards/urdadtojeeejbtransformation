package org.urdadmde.mapping.javaee.example.training.traininginstitution.finance;

import org.urdadmde.mapping.javaee.urdadstandardprofile.storage.Entity;

@javax.persistence.Entity
public class InvoiceItem extends Entity
{

	public Double getCost()
	{
		return cost;
	}

	public void setCost(Double cost)
	{
		this.cost = cost;
	}

	public Integer getQuantity()
	{
		return quantity;
	}

	public void setQuantity(Integer quantity)
	{
		this.quantity = quantity;
	}

	public String getChargeableIdentifier()
	{
		return chargeableIdentifier;
	}

	public void setChargeableIdentifier(String chargeableIdentifier)
	{
		this.chargeableIdentifier = chargeableIdentifier;
	}

	private Double cost;
	private Integer quantity;
	private String chargeableIdentifier;

}