package org.urdadmde.mapping.javaee.example.training.shareholder;

import javax.ejb.Stateless;

@Stateless
public class ShareholderBean implements ShareholderLocal, ShareholderRemote{}