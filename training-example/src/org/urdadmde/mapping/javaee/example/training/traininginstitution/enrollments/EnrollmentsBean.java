package org.urdadmde.mapping.javaee.example.training.traininginstitution.enrollments;

import java.util.Collection;
import java.util.concurrent.Future;
import javax.annotation.Resource;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import org.jboss.ejb3.annotation.IgnoreDependency;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment.validation.CheckStudentSatisfiesEnrollmentPrerequisitesRequest;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment.validation.CheckStudentSatisfiesEnrollmentPrerequisitesResult;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment.validation.StudentSatisfiesEnrollmentPrerequisites;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment.validation.ValidationLocal;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.finance.FinanceLocal;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.finance.FinancialPrerequisitesNotSatisfiedException;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.finance.InvoiceItem;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.finance.IssueInvoiceRequest;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.finance.IssueInvoiceResult;

@Stateless
public class EnrollmentsBean implements EnrollmentsLocal, EnrollmentsRemote
{

	@Override
	public EnrollForPresentationResult enrollForPresentation(EnrollForPresentationRequest
		enrollForPresentationRequest) throws FinancialPrerequisitesNotSatisfiedException,
		EnrollmentPrerequisitesNotSatisfiedException
	{
		CheckStudentSatisfiesEnrollmentPrerequisitesRequest
			checkStudentSatisfiesEnrollmentPrerequisitesRequest = new
			CheckStudentSatisfiesEnrollmentPrerequisitesRequest();
		checkStudentSatisfiesEnrollmentPrerequisitesRequest.setStudentIdentifier(enrollForPresentationRequest
			.getStudentIdentifier());
		checkStudentSatisfiesEnrollmentPrerequisitesRequest.setPresentationIdentifier
			(enrollForPresentationRequest.getPresentationIdentifier());
		CheckStudentSatisfiesEnrollmentPrerequisitesResult checkStudentSatisfiesEnrollmentPrerequisitesResult
			= validation.checkStudentSatisfiesEnrollmentPrerequisites
			(checkStudentSatisfiesEnrollmentPrerequisitesRequest);
		if (checkStudentSatisfiesEnrollmentPrerequisitesResult instanceof
			StudentSatisfiesEnrollmentPrerequisites){
			IssueInvoiceRequest issueInvoiceRequest = new IssueInvoiceRequest();
			issueInvoiceRequest.setClientIdentifier(enrollForPresentationRequest.getClientIdentifier());
			InvoiceItem invoiceItem = new InvoiceItem();
			invoiceItem.setChargeableIdentifier(enrollForPresentationRequest.getPresentationIdentifier());
			Collection<InvoiceItem> invoiceItems = issueInvoiceRequest.getInvoiceItems();
			invoiceItems.add(invoiceItem);
			issueInvoiceRequest.setInvoiceItems(invoiceItems);
			IssueInvoiceResult issueInvoiceResult = null;
			try
			{
				issueInvoiceResult = finance.issueInvoice(issueInvoiceRequest);
			}
			catch (FinancialPrerequisitesNotSatisfiedException e)
			{
				throw new FinancialPrerequisitesNotSatisfiedException();
			}
			PerformEnrollmentRequest performEnrollmentRequest = new PerformEnrollmentRequest();
			performEnrollmentRequest.setPersonIdentifier(enrollForPresentationRequest
				.getStudentIdentifier());
			performEnrollmentRequest.setPresentationIdentifier(enrollForPresentationRequest
				.getPresentationIdentifier());
			PerformEnrollmentResult performEnrollmentResult = getEnrollments().performEnrollment
				(performEnrollmentRequest);
			EnrollForPresentationResult enrollForPresentationResult = new EnrollForPresentationResult();
			enrollForPresentationResult.setInvoice(issueInvoiceResult.getInvoice());
			enrollForPresentationResult.setProofOfEnrollment(performEnrollmentResult.getProofOfEnrollment());
			enrollForPresentationResult.setStudyGuide(performEnrollmentResult.getStudyGuide());
            return enrollForPresentationResult;
		}
		else{
			throw new EnrollmentPrerequisitesNotSatisfiedException();
		}
	}

	@Override
	@Asynchronous
	public Future<EnrollForPresentationResult> enrollForPresentationAsynchronously
		(EnrollForPresentationRequest enrollForPresentationRequest) throws 
		FinancialPrerequisitesNotSatisfiedException, EnrollmentPrerequisitesNotSatisfiedException
	{
		return new AsyncResult<EnrollForPresentationResult>(enrollForPresentation
			(enrollForPresentationRequest));
	}
	
	@Override
	public PerformEnrollmentResult performEnrollment(PerformEnrollmentRequest performEnrollmentRequest)
	{
		return null;
	}

	@Override
	@Asynchronous
	public Future<PerformEnrollmentResult> performEnrollmentAsynchronously(PerformEnrollmentRequest 
		performEnrollmentRequest)
	{
		return new AsyncResult<PerformEnrollmentResult>(performEnrollment(performEnrollmentRequest));
	}
	
	private EnrollmentsLocal getEnrollments(){
		return sessionContext.getBusinessObject(EnrollmentsLocal.class);
	}

	@EJB
	@IgnoreDependency
	private ValidationLocal validation;
	@EJB
	@IgnoreDependency
	private FinanceLocal finance;
	@Resource
	protected SessionContext sessionContext;

}