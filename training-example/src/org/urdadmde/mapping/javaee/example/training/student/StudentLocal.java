package org.urdadmde.mapping.javaee.example.training.student;

import javax.ejb.Local;

@Local
public interface StudentLocal extends Student{}