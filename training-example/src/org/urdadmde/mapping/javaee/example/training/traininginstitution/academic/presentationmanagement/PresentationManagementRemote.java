package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.presentationmanagement;

import javax.ejb.Local;

@Local
public interface PresentationManagementRemote extends PresentationManagement{}