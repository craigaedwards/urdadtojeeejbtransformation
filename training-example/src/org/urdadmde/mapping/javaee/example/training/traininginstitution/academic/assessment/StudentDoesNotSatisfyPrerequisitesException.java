package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment;

public class StudentDoesNotSatisfyPrerequisitesException extends Exception{}