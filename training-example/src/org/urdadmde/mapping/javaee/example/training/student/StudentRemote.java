package org.urdadmde.mapping.javaee.example.training.student;

import javax.ejb.Remote;

@Remote
public interface StudentRemote extends Student{}