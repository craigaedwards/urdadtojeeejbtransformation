package org.urdadmde.mapping.javaee.example.training.creditregulator;

import javax.ejb.Stateless;

@Stateless
public class CreditRegulatorBean implements CreditRegulatorLocal, CreditRegulatorRemote{}