package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.presentationmanagement;

import javax.ejb.Remote;

@Remote
public interface PresentationManagementLocal extends PresentationManagement{}