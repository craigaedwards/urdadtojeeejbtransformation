package org.urdadmde.mapping.javaee.example.training.shareholder;

import javax.ejb.Local;

@Local
public interface ShareholderLocal extends Shareholder{}