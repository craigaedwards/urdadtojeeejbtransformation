package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.studyguides;

import javax.ejb.Remote;

@Remote
public interface StudyGuidesRemote extends StudyGuides{}