package org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement;

import java.io.Serializable;

public class ProvideRegistrationDetailsResult implements Serializable{

	public Person getPersonDetails()
	{
		return personDetails;
	}

	public void setPersonDetails(Person personDetails)
	{
		this.personDetails = personDetails;
	}

	private Person personDetails;

}