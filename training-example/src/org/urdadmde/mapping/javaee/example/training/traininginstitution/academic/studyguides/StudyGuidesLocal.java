package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.studyguides;

import javax.ejb.Local;

@Local
public interface StudyGuidesLocal extends StudyGuides{}