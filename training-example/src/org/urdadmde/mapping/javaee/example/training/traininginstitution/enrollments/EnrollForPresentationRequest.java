package org.urdadmde.mapping.javaee.example.training.traininginstitution.enrollments;

import java.io.Serializable;

public class EnrollForPresentationRequest implements Serializable
{

	public String getPresentationIdentifier()
	{
		return presentationIdentifier;
	}

	public void setPresentationIdentifier(String presentationIdentifier)
	{
		this.presentationIdentifier = presentationIdentifier;
	}

	public String getStudentIdentifier()
	{
		return studentIdentifier;
	}

	public void setStudentIdentifier(String studentIdentifier)
	{
		this.studentIdentifier = studentIdentifier;
	}

	public String getClientIdentifier()
	{
		return clientIdentifier;
	}

	public void setClientIdentifier(String clientIdentifier)
	{
		this.clientIdentifier = clientIdentifier;
	}

	private String presentationIdentifier;
	private String studentIdentifier;
	private String clientIdentifier;

}