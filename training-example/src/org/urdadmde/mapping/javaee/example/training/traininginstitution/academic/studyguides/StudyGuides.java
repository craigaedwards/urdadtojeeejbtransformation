package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.studyguides;

import java.util.concurrent.Future;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement.PersonNotRegisteredException;

public interface StudyGuides
{

	public ProvideStudyGuideResult provideStudyGuide(ProvideStudyGuideRequest provideStudyGuideRequest) throws
		PersonNotRegisteredException;

	public Future<ProvideStudyGuideResult> provideStudyGuideAsynchronously(ProvideStudyGuideRequest
		provideStudyGuideRequest)throws	PersonNotRegisteredException;
	
}