package org.urdadmde.mapping.javaee.example.training.traininginstitution.enrollments.presentationmanagement;

import javax.ejb.Local;

@Local
public interface PresentationManagementLocal extends PresentationManagement{}