package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic;

import javax.ejb.Local;

@Local
public interface AcademicLocal extends Academic{}