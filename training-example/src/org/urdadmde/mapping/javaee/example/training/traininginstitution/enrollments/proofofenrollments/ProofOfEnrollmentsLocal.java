package org.urdadmde.mapping.javaee.example.training.traininginstitution.enrollments.proofofenrollments;

import javax.ejb.Local;

@Local
public interface ProofOfEnrollmentsLocal extends ProofOfEnrollments{}