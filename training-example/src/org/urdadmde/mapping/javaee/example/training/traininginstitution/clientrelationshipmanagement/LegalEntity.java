package org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import org.urdadmde.mapping.javaee.urdadstandardprofile.storage.Entity;

@javax.persistence.Entity
public class LegalEntity extends Entity
{

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Address getLegalAddress()
	{
		return legalAddress;
	}

	public void setLegalAddress(Address legalAddress)
	{
		this.legalAddress = legalAddress;
	}

	private String name;
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "legalAddress")
	private Address legalAddress;

}