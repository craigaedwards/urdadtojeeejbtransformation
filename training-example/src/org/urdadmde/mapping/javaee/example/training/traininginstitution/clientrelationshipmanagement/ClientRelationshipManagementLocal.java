package org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement;

import javax.ejb.Local;

@Local
public interface ClientRelationshipManagementLocal extends ClientRelationshipManagement{}