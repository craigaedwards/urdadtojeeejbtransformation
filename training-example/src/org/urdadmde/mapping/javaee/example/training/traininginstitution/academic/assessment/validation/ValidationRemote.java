package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment.validation;

import javax.ejb.Remote;

@Remote
public interface ValidationRemote extends Validation{}