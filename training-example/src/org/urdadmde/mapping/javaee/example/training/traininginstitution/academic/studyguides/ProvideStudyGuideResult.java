package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.studyguides;

import java.io.Serializable;

public class ProvideStudyGuideResult implements Serializable
{

	public StudyGuide getStudyGuide()
	{
		return studyGuide;
	}

	public void setStudyGuide(StudyGuide studyGuide)
	{
		this.studyGuide = studyGuide;
	}

	private StudyGuide studyGuide;

}