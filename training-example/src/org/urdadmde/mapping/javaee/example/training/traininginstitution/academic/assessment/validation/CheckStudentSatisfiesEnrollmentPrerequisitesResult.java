package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment.validation;

import java.io.Serializable;

public abstract class CheckStudentSatisfiesEnrollmentPrerequisitesResult implements Serializable
{

	public String getPersonIdentifier()
	{
		return personIdentifier;
	}
	
	public void setPersonIdentifier(String personIdentifier)
	{
		this.personIdentifier = personIdentifier;
	}
	
	public String getPresentationIdentifier()
	{
		return presentationIdentifier;
	}
	
	public void setPresentationIdentifier(String presentationIdentifier)
	{
		this.presentationIdentifier = presentationIdentifier;
	}
	
	private String personIdentifier;
	private String presentationIdentifier;

}