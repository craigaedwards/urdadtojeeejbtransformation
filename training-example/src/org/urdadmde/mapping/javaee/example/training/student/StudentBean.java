package org.urdadmde.mapping.javaee.example.training.student;

import javax.ejb.Stateless;

@Stateless
public class StudentBean implements StudentLocal, StudentRemote{}