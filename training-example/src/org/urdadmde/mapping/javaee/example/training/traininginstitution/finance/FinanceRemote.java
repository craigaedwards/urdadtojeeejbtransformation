package org.urdadmde.mapping.javaee.example.training.traininginstitution.finance;

import javax.ejb.Remote;

@Remote
public interface FinanceRemote extends Finance{}