package org.urdadmde.mapping.javaee.example.training.traininginstitution.enrollments.presentationmanagement;

import java.util.concurrent.Future;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement.PersonNotRegisteredException;

@Stateless(name="org.urdadmde.implementationmapping.javaee.examples.training.traininginstitution.enrollments" +
	".presentationmanagement.PresentationManagementBean")
public class PresentationManagementBean implements PresentationManagementLocal, PresentationManagementRemote
{

	@Override
	public EnlistStudentForPresentationResult enlistStudentForPresentation(EnlistStudentForPresentationRequest
		enlistStudentForPresentationRequest) throws PersonNotRegisteredException{
		return null;
	}

	@Override
	@Asynchronous
	public Future<EnlistStudentForPresentationResult> enlistStudentForPresentationAsynchronously
		(EnlistStudentForPresentationRequest enlistStudentForPresentationRequest) throws 
		PersonNotRegisteredException
	{
		return new AsyncResult<EnlistStudentForPresentationResult>(enlistStudentForPresentation
			(enlistStudentForPresentationRequest));
	}

}