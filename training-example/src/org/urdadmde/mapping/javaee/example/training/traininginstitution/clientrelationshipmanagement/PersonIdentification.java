package org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class PersonIdentification extends LegalEntityIdentification
{

	public String getPerson()
	{
		return person;
	}

	public void setPerson(String person)
	{
		this.person = person;
	}

	private String person;

}