package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment;

import javax.ejb.Local;

@Local
public interface AssessmentLocal extends Assessment{}