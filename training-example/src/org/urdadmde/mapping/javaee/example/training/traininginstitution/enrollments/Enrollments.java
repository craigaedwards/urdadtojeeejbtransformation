package org.urdadmde.mapping.javaee.example.training.traininginstitution.enrollments;

import java.util.concurrent.Future;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.finance.FinancialPrerequisitesNotSatisfiedException;

public interface Enrollments
{

	public EnrollForPresentationResult enrollForPresentation(EnrollForPresentationRequest
		enrollForPresentationRequest) throws FinancialPrerequisitesNotSatisfiedException,
		EnrollmentPrerequisitesNotSatisfiedException;
	
	public Future<EnrollForPresentationResult> enrollForPresentationAsynchronously
		(EnrollForPresentationRequest enrollForPresentationRequest) throws 
		FinancialPrerequisitesNotSatisfiedException, EnrollmentPrerequisitesNotSatisfiedException;

	public PerformEnrollmentResult performEnrollment(PerformEnrollmentRequest performEnrollmentRequest);

	public Future<PerformEnrollmentResult> performEnrollmentAsynchronously(PerformEnrollmentRequest 
		performEnrollmentRequest);
	
}