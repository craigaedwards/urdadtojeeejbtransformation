package org.urdadmde.mapping.javaee.example.training.traininginstitution.enrollments.proofofenrollments;

import java.util.concurrent.Future;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment.CheckStudentSatisfiesCoursePrerequisitesResult;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement.PersonNotRegisteredException;

@Stateless
public class ProofOfEnrollmentsBean implements ProofOfEnrollmentsLocal, ProofOfEnrollmentsRemote
{

	@Override
	public ProvideProofOfEnrollmentResult provideProofOfEnrollment(ProvideProofOfEnrollmentRequest 
			provideProofOfEnrollmentRequest) throws PersonNotRegisteredException
	{
		return null;
	}
	
	@Override
	@Asynchronous
	public Future<ProvideProofOfEnrollmentResult> provideProofOfEnrollmentAsynchronously
		(ProvideProofOfEnrollmentRequest provideProofOfEnrollmentRequest) throws PersonNotRegisteredException
	{
		return new AsyncResult<ProvideProofOfEnrollmentResult>
			(provideProofOfEnrollment(provideProofOfEnrollmentRequest));
	}

}