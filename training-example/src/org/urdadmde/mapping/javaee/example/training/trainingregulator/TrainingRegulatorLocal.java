package org.urdadmde.mapping.javaee.example.training.trainingregulator;

import javax.ejb.Local;

@Local
public interface TrainingRegulatorLocal extends TrainingRegulator{}