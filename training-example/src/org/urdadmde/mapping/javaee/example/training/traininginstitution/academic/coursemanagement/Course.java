package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.coursemanagement;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

public class Course implements Serializable
{

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Collection<String> getCoursePrerequisiteIdentifiers()
	{
		return coursePrerequisiteIdentifiers;
	}

	public void setCoursePrerequisiteIdentifiers(Collection<String> coursePrerequisiteIdentifiers)
	{
		this.coursePrerequisiteIdentifiers = coursePrerequisiteIdentifiers;
	}

	private String name;
	private Collection<String> coursePrerequisiteIdentifiers = new LinkedList<String>();

}