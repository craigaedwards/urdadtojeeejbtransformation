package org.urdadmde.mapping.javaee.example.training.traininginstitution.finance;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

public class IssueInvoiceRequest implements Serializable{

	public String getClientIdentifier()
	{
		return clientIdentifier;
	}

	public void setClientIdentifier(String clientIdentifier)
	{
		this.clientIdentifier = clientIdentifier;
	}

	public Collection<InvoiceItem> getInvoiceItems()
	{
		return invoiceItems;
	}

	public void setInvoiceItems(Collection<InvoiceItem> invoiceItems)
	{
		this.invoiceItems = invoiceItems;
	}

	private String clientIdentifier;
	private Collection<InvoiceItem> invoiceItems = new LinkedList<InvoiceItem>();

}