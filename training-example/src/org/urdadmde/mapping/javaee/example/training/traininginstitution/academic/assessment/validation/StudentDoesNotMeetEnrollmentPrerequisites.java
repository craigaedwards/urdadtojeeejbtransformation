package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment.validation;

import java.util.Collection;
import java.util.LinkedList;

public class StudentDoesNotMeetEnrollmentPrerequisites extends 
	CheckStudentSatisfiesEnrollmentPrerequisitesResult
{

	public Collection<String> getReasons()
	{
		return reasons;
	}

	public void setReasons(Collection<String> reasons)
	{
		this.reasons = reasons;
	}

	private Collection<String> reasons = new LinkedList<String>();

}