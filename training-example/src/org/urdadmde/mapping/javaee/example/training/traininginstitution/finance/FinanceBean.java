package org.urdadmde.mapping.javaee.example.training.traininginstitution.finance;

import java.util.concurrent.Future;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;

@Stateless
public class FinanceBean implements FinanceLocal, FinanceRemote
{

	@Override
	public IssueInvoiceResult issueInvoice(IssueInvoiceRequest issueInvoiceRequest) throws
		FinancialPrerequisitesNotSatisfiedException
	{
		return null;
	}
	
	@Override
	@Asynchronous
	public Future<IssueInvoiceResult> issueInvoiceAsynchronously(IssueInvoiceRequest 
		issueInvoiceRequest) throws FinancialPrerequisitesNotSatisfiedException
	{
		return new AsyncResult<IssueInvoiceResult> (issueInvoice(issueInvoiceRequest));
	}

	@Override
	public ProvideInvoiceForEnrollmentResult provideInvoiceForEnrollment(ProvideInvoiceForEnrollmentResult
		provideInvoiceForEnrollmentResult) throws NoInvoiceIssuedForEnrollmentException
	{
		return null;
	}
	
	@Override
	@Asynchronous
	public Future<ProvideInvoiceForEnrollmentResult> provideInvoiceForEnrollmentAsynchronously
		(ProvideInvoiceForEnrollmentResult provideInvoiceForEnrollmentResult) throws 
		NoInvoiceIssuedForEnrollmentException
	{
		return new AsyncResult<ProvideInvoiceForEnrollmentResult> (provideInvoiceForEnrollment
			(provideInvoiceForEnrollmentResult));
	}

}