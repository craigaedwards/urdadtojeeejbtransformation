package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment;

import java.io.Serializable;

public class CheckStudentSatisfiesCoursePrerequisitesRequest implements Serializable
{

	public String getStudentIdentifier() 
	{
		return studentIdentifier;
	}
	
	public void setStudentIdentifier(String studentIdentifier) 
	{
		this.studentIdentifier = studentIdentifier;
	}
	
	public String getPresentationIdentifier() 
	{
		return presentationIdentifier;
	}
	
	public void setPresentationIdentifier(String presentationIdentifier) 
	{
		this.presentationIdentifier = presentationIdentifier;
	}
	
	private String studentIdentifier;
	private String presentationIdentifier;

}