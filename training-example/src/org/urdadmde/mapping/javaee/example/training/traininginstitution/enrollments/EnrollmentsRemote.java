package org.urdadmde.mapping.javaee.example.training.traininginstitution.enrollments;

import javax.ejb.Remote;

@Remote
public interface EnrollmentsRemote extends Enrollments{}