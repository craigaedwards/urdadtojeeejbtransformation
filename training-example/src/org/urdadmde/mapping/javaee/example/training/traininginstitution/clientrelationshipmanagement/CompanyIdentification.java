package org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement;

@javax.persistence.Entity
public class CompanyIdentification extends LegalEntityIdentification
{

	public String getCompanyRegistrationNumber()
	{
		return companyRegistrationNumber;
	}

	public void setCompanyRegistrationNumber(String companyRegistrationNumber)
	{
		this.companyRegistrationNumber = companyRegistrationNumber;
	}

	private String companyRegistrationNumber;

}