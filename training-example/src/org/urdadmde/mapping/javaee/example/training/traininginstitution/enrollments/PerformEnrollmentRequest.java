
package org.urdadmde.mapping.javaee.example.training.traininginstitution.enrollments;

import java.io.Serializable;

import org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.presentationmanagement.Presentation;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement.Person;

public class PerformEnrollmentRequest implements Serializable
{

	public String getPresentationIdentifier()
	{
		return presentationIdentifier;
	}

	public void setPresentationIdentifier(String presentationIdentifier)
	{
		this.presentationIdentifier = presentationIdentifier;
	}

	public String getPersonIdentifier()
	{
		return personIdentifier;
	}

	public void setPersonIdentifier(String personIdentifier)
	{
		this.personIdentifier = personIdentifier;
	}

	private String presentationIdentifier;
	private String personIdentifier;

}