package org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement;

import java.util.Collection;
import java.util.LinkedList;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.JoinColumn;
import org.urdadmde.mapping.javaee.urdadstandardprofile.storage.Entity;

@javax.persistence.Entity
public class Address extends Entity
{
	
	public Collection<String> getAddressLines()
	{
		return addressLines;
	}

	public void setAddressLines(Collection<String> addressLines)
	{
		this.addressLines = addressLines;
	}

	@ElementCollection
	@CollectionTable(name = "addresslines", joinColumns = {@JoinColumn(name = "address")})
	@Column(name = "addressline")
	private Collection<String> addressLines = new LinkedList<String>();

}