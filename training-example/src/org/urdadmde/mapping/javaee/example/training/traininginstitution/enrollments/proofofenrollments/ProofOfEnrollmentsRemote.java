package org.urdadmde.mapping.javaee.example.training.traininginstitution.enrollments.proofofenrollments;

import javax.ejb.Remote;

@Remote
public interface ProofOfEnrollmentsRemote extends ProofOfEnrollments{}