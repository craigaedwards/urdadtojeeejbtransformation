package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.coursemanagement;

import javax.ejb.Local;

@Local
public interface CourseManagementLocal extends CourseManagement{}