package org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement;

import java.util.concurrent.Future;

public interface ClientRelationshipManagement
{

	public ProvideRegistrationDetailsResult provideRegistrationDetails(ProvideRegistrationDetailsRequest
		provideRegistrationDetailsRequest) throws PersonNotRegisteredException;
	
	public Future<ProvideRegistrationDetailsResult> provideRegistrationDetailsAsynchronously
		(ProvideRegistrationDetailsRequest provideRegistrationDetailsRequest) throws 
		PersonNotRegisteredException;

}