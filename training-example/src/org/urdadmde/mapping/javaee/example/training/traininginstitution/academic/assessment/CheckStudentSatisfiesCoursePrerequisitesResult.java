package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment;

import java.io.Serializable;

public class CheckStudentSatisfiesCoursePrerequisitesResult implements Serializable
{

	public boolean isPrerequisitesSatisfied()
	{
		return prerequisitesSatisfied;
	}

	public void setPrerequisitesSatisfied(boolean prerequisitesSatisfied)
	{
		this.prerequisitesSatisfied = prerequisitesSatisfied;
	}

	private boolean prerequisitesSatisfied;

}