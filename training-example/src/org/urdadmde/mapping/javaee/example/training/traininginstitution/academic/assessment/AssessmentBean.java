package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment;

import java.util.concurrent.Future;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;

@Stateless
public class AssessmentBean implements AssessmentLocal, AssessmentRemote
{

	@Override
	public CheckStudentSatisfiesCoursePrerequisitesResult checkStudentSatisfiesCoursePrerequisites
		(CheckStudentSatisfiesCoursePrerequisitesRequest checkStudentSatisfiesCoursePrerequisitesRequest){
		return null;
	}

	@Override
	@Asynchronous
	public Future<CheckStudentSatisfiesCoursePrerequisitesResult>
		checkStudentSatisfiesCoursePrerequisitesAsynchronously(CheckStudentSatisfiesCoursePrerequisitesRequest
		checkStudentSatisfiesCoursePrerequisitesRequest){
		return new AsyncResult<CheckStudentSatisfiesCoursePrerequisitesResult>
			(checkStudentSatisfiesCoursePrerequisites(checkStudentSatisfiesCoursePrerequisitesRequest));
	}

}