package org.urdadmde.mapping.javaee.example.training.traininginstitution.finance;

import java.io.Serializable;

public class IssueInvoiceResult implements Serializable{

	public Invoice getInvoice()
	{
		return (Invoice) invoice;
	}

	public void setInvoice(Invoice invoice)
	{
		this.invoice = (Invoice) invoice;
	}

	private Invoice invoice;

}