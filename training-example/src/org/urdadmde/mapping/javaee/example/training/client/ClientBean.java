package org.urdadmde.mapping.javaee.example.training.client;

import javax.ejb.Stateless;

@Stateless
public class ClientBean implements ClientLocal, ClientRemote{}