package org.urdadmde.mapping.javaee.example.training.traininginstitution.finance;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement.LegalEntity;
import org.urdadmde.mapping.javaee.urdadstandardprofile.storage.Entity;

@javax.persistence.Entity
public class Invoice extends Entity
{

	public String getInvoiceNumber()
	{
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber)
	{
		this.invoiceNumber = invoiceNumber;
	}

	public Date getInvoiceDate()
	{
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate)
	{
		this.invoiceDate = invoiceDate;
	}

	public LegalEntity getClientDetails()
	{
		return clientDetails;
	}

	public void setClientDetails(LegalEntity clientDetails)
	{
		this.clientDetails = clientDetails;
	}

	public Collection<InvoiceItem> getInvoiceItems()
	{
		return invoiceItems;
	}

	public void setInvoiceItems(Collection<InvoiceItem> invoiceItems)
	{
		this.invoiceItems = invoiceItems;
	}

	private String invoiceNumber;
	private Date invoiceDate;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "clientDetails")
	private LegalEntity clientDetails;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "invoice")
	private Collection<InvoiceItem> invoiceItems = new LinkedList<InvoiceItem>();

}