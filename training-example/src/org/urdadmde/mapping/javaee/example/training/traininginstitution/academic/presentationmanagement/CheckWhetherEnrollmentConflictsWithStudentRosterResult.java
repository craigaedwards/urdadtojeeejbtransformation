package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.presentationmanagement;

import java.io.Serializable;

public class CheckWhetherEnrollmentConflictsWithStudentRosterResult implements Serializable
{

	public boolean isNoRosterConflict()
	{
		return noRosterConflict;
	}

	public void setNoRosterConflict(boolean noRosterConflict)
	{
		this.noRosterConflict = noRosterConflict;
	}

	private boolean noRosterConflict;

}