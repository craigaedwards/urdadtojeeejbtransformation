package org.urdadmde.mapping.javaee.example.training.creditregulator;

import javax.ejb.Local;

@Local
public interface CreditRegulatorLocal extends CreditRegulator{}