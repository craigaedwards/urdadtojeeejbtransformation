package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment;

import javax.ejb.Remote;

@Remote
public interface AssessmentRemote extends Assessment{}