package org.urdadmde.mapping.javaee.example.training.traininginstitution.enrollments.presentationmanagement;

import javax.ejb.Remote;

@Remote
public interface PresentationManagementRemote extends PresentationManagement{}