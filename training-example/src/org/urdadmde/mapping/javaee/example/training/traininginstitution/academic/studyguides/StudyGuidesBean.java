package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.studyguides;

import java.util.concurrent.Future;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement.PersonNotRegisteredException;

@Stateless
public class StudyGuidesBean implements StudyGuidesLocal, StudyGuidesRemote
{

	@Override
	public ProvideStudyGuideResult provideStudyGuide(ProvideStudyGuideRequest provideStudyGuideRequest)
		throws PersonNotRegisteredException
	{
		return null;
	}
	
	@Override
	@Asynchronous
	public Future<ProvideStudyGuideResult> provideStudyGuideAsynchronously(ProvideStudyGuideRequest
		provideStudyGuideRequest)throws	PersonNotRegisteredException
	{
		return new AsyncResult<ProvideStudyGuideResult>(provideStudyGuide(provideStudyGuideRequest));
	}

}