package org.urdadmde.mapping.javaee.example.training.traininginstitution.finance;

public class NoInvoiceIssuedForEnrollmentException extends Exception{}