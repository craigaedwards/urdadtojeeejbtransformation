package org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement;

@javax.persistence.Entity
public class Passport extends PersonIdentification
{

	public String getPassportNumber()
	{
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber)
	{
		this.passportNumber = passportNumber;
	}

	private String passportNumber;

}