package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.presentationmanagement;

import java.util.concurrent.Future;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;

@Stateless(name="org.urdadmde.mapping.javaee.examples.training.traininginstitution.academic" +
	".presentationmanagement.PresentationManagementBean")
public class PresentationManagementBean implements PresentationManagementLocal, PresentationManagementRemote
{

	@Override
	public CheckWhetherEnrollmentConflictsWithStudentRosterResult
		checkWhetherEnrollmentConflictsWithStudentRoster
		(CheckWhetherEnrollmentConflictsWithStudentRosterRequest
		checkWhetherEnrollmentConflictsWithStudentRosterRequest)
	{
		return null;
	}

	@Override
	@Asynchronous
	public Future<CheckWhetherEnrollmentConflictsWithStudentRosterResult>
		checkWhetherEnrollmentConflictsWithStudentRosterAsynchronously
		(CheckWhetherEnrollmentConflictsWithStudentRosterRequest
		checkWhetherEnrollmentConflictsWithStudentRosterRequest)
	{
		return new AsyncResult<CheckWhetherEnrollmentConflictsWithStudentRosterResult>
			(checkWhetherEnrollmentConflictsWithStudentRoster
			(checkWhetherEnrollmentConflictsWithStudentRosterRequest));
	}

}