package org.urdadmde.mapping.javaee.example.training.traininginstitution.enrollments;

public class EnrollmentPrerequisitesNotSatisfiedException extends Exception{}