package org.urdadmde.mapping.javaee.example.training.trainingregulator;

import javax.ejb.Stateless;

@Stateless
public class TrainingRegulatorBean implements TrainingRegulatorLocal, TrainingRegulatorRemote{}