package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment.validation;

import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.jboss.ejb3.annotation.IgnoreDependency;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment.AssessmentLocal;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment.CheckStudentSatisfiesCoursePrerequisitesRequest;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.assessment.CheckStudentSatisfiesCoursePrerequisitesResult;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.presentationmanagement.CheckWhetherEnrollmentConflictsWithStudentRosterRequest;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.presentationmanagement.CheckWhetherEnrollmentConflictsWithStudentRosterResult;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.presentationmanagement.PresentationManagementLocal;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement.ClientRelationshipManagementLocal;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement.PersonNotRegisteredException;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement.ProvideRegistrationDetailsRequest;

@Stateless
public class ValidationBean implements ValidationLocal, ValidationRemote
{

	@Override
	public CheckStudentSatisfiesEnrollmentPrerequisitesResult checkStudentSatisfiesEnrollmentPrerequisites(
		CheckStudentSatisfiesEnrollmentPrerequisitesRequest
		checkStudentSatisfiesEnrollmentPrerequisitesRequest)
	{
		ProvideRegistrationDetailsRequest provideRegistrationDetailsRequest = new
			ProvideRegistrationDetailsRequest();
		provideRegistrationDetailsRequest.setPersonIdentifier
			(checkStudentSatisfiesEnrollmentPrerequisitesRequest.getStudentIdentifier());
		try
		{
			clientRelationshipManagement.provideRegistrationDetails
				(provideRegistrationDetailsRequest);
		}
		catch (PersonNotRegisteredException e)
		{
			StudentDoesNotMeetEnrollmentPrerequisites checkStudentSatisfiesEnrollmentPrerequisitesResult =
				new StudentDoesNotMeetEnrollmentPrerequisites();			
			return checkStudentSatisfiesEnrollmentPrerequisitesResult;
		}
		CheckStudentSatisfiesCoursePrerequisitesRequest checkStudentSatisfiesCoursePrerequisitesRequest =
			new CheckStudentSatisfiesCoursePrerequisitesRequest();
		checkStudentSatisfiesCoursePrerequisitesRequest.setStudentIdentifier
			(checkStudentSatisfiesEnrollmentPrerequisitesRequest.getStudentIdentifier());
		checkStudentSatisfiesCoursePrerequisitesRequest.setPresentationIdentifier
			(checkStudentSatisfiesEnrollmentPrerequisitesRequest.getPresentationIdentifier());
		Future<CheckStudentSatisfiesCoursePrerequisitesResult>
			asynchronousCheckStudentSatisfiesCoursePrerequisitesResult = assessment
			.checkStudentSatisfiesCoursePrerequisitesAsynchronously
			(checkStudentSatisfiesCoursePrerequisitesRequest);
		CheckWhetherEnrollmentConflictsWithStudentRosterRequest
			checkWhetherEnrollmentConflictsWithStudentRosterRequest = new
			CheckWhetherEnrollmentConflictsWithStudentRosterRequest();
		checkWhetherEnrollmentConflictsWithStudentRosterRequest.setStudentIdentifier
			(checkStudentSatisfiesEnrollmentPrerequisitesRequest.getStudentIdentifier());
		checkWhetherEnrollmentConflictsWithStudentRosterRequest.setPresentationIdentifier
			(checkStudentSatisfiesEnrollmentPrerequisitesRequest.getPresentationIdentifier());
		Future<CheckWhetherEnrollmentConflictsWithStudentRosterResult>
			asynchronousCheckWhetherEnrollmentConflictsWithStudentRosterResult = presentationManagement
			.checkWhetherEnrollmentConflictsWithStudentRosterAsynchronously
			(checkWhetherEnrollmentConflictsWithStudentRosterRequest);
		while ((!asynchronousCheckStudentSatisfiesCoursePrerequisitesResult.isDone()) &&
			(!asynchronousCheckWhetherEnrollmentConflictsWithStudentRosterResult.isDone())){
			try
			{
				Thread.sleep(1000); //FIXME Consider timeout
			}
			catch (InterruptedException e)
			{
				throw new Error(e);
			}
		}
		CheckStudentSatisfiesCoursePrerequisitesResult checkStudentSatisfiesCoursePrerequisitesResult = null;
		CheckWhetherEnrollmentConflictsWithStudentRosterResult
			checkWhetherEnrollmentConflictsWithStudentRosterResult = null;
		try
		{
			checkStudentSatisfiesCoursePrerequisitesResult =
				asynchronousCheckStudentSatisfiesCoursePrerequisitesResult.get();
			checkWhetherEnrollmentConflictsWithStudentRosterResult =
				asynchronousCheckWhetherEnrollmentConflictsWithStudentRosterResult.get();
		}
		catch (InterruptedException e)
		{
			throw new Error(e);
		}
		catch (ExecutionException e)
		{
			throw new Error(e);
		}
		if ((checkStudentSatisfiesCoursePrerequisitesResult.isPrerequisitesSatisfied())	&&
			(checkWhetherEnrollmentConflictsWithStudentRosterResult.isNoRosterConflict()))
		{
			StudentSatisfiesEnrollmentPrerequisites checkStudentSatisfiesEnrollmentPrerequisitesResult =
				new StudentSatisfiesEnrollmentPrerequisites();
			return checkStudentSatisfiesEnrollmentPrerequisitesResult;
		}
		else
		{
			StudentDoesNotMeetEnrollmentPrerequisites checkStudentSatisfiesEnrollmentPrerequisitesResult
				= new StudentDoesNotMeetEnrollmentPrerequisites();
			if (!checkStudentSatisfiesCoursePrerequisitesResult.isPrerequisitesSatisfied()){
				Collection<String> reasons = checkStudentSatisfiesEnrollmentPrerequisitesResult.getReasons();
				reasons.add("Course prerequisites not satisfied.");
				checkStudentSatisfiesEnrollmentPrerequisitesResult.setReasons(reasons);
				checkStudentSatisfiesEnrollmentPrerequisitesResult.setReasons(reasons);
			}
			if (!checkWhetherEnrollmentConflictsWithStudentRosterResult.isNoRosterConflict()){
				Collection<String> reasons = checkStudentSatisfiesEnrollmentPrerequisitesResult.getReasons();
				reasons.add("Enrollment conflicts with student roster.");
				checkStudentSatisfiesEnrollmentPrerequisitesResult.setReasons(reasons);
				checkStudentSatisfiesEnrollmentPrerequisitesResult.setReasons(reasons);
			}
			 return checkStudentSatisfiesEnrollmentPrerequisitesResult;
		}
	}
	
	@Override
	@Asynchronous
	public Future<CheckStudentSatisfiesEnrollmentPrerequisitesResult> 
		checkStudentSatisfiesEnrollmentPrerequisitesAsynchronously
		(CheckStudentSatisfiesEnrollmentPrerequisitesRequest
		checkStudentSatisfiesEnrollmentPrerequisitesRequest)
	{
		return new AsyncResult<CheckStudentSatisfiesEnrollmentPrerequisitesResult>
			(checkStudentSatisfiesEnrollmentPrerequisites
			(checkStudentSatisfiesEnrollmentPrerequisitesRequest));
	}

	@EJB
	@IgnoreDependency
	private ClientRelationshipManagementLocal clientRelationshipManagement;
	@EJB
	@IgnoreDependency
	private AssessmentLocal assessment;
	@EJB
	@IgnoreDependency
	private PresentationManagementLocal presentationManagement;

}