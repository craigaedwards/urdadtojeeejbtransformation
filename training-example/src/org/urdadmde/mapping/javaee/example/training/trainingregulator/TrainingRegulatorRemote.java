package org.urdadmde.mapping.javaee.example.training.trainingregulator;

import javax.ejb.Remote;

@Remote
public interface TrainingRegulatorRemote extends TrainingRegulator{}