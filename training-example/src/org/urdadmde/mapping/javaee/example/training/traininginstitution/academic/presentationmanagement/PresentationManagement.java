package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.presentationmanagement;

import java.util.concurrent.Future;

public interface PresentationManagement
{

	public CheckWhetherEnrollmentConflictsWithStudentRosterResult
		checkWhetherEnrollmentConflictsWithStudentRoster
		(CheckWhetherEnrollmentConflictsWithStudentRosterRequest
		checkWhetherEnrollmentConflictsWithStudentRosterRequest);

	public Future<CheckWhetherEnrollmentConflictsWithStudentRosterResult>
		checkWhetherEnrollmentConflictsWithStudentRosterAsynchronously
		(CheckWhetherEnrollmentConflictsWithStudentRosterRequest
		checkWhetherEnrollmentConflictsWithStudentRosterRequest);

}