package org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement;

import javax.persistence.MappedSuperclass;
import org.urdadmde.mapping.javaee.urdadstandardprofile.storage.Entity;

@MappedSuperclass
public abstract class LegalEntityIdentification extends Entity
{

	public String getIssuingOrganization()
	{
		return issuingOrganization;
	}

	public void setIssuingOrganisation(String issuingOrganization)
	{
		this.issuingOrganization = issuingOrganization;
	}

	private String issuingOrganization;

}