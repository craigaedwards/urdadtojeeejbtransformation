package org.urdadmde.mapping.javaee.example.training.traininginstitution.finance;

import javax.ejb.Local;

@Local
public interface FinanceLocal extends Finance{}