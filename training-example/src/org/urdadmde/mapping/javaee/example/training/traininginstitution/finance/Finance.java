package org.urdadmde.mapping.javaee.example.training.traininginstitution.finance;

import java.util.concurrent.Future;

public interface Finance
{

	public IssueInvoiceResult issueInvoice(IssueInvoiceRequest issueInvoiceRequest) throws
		FinancialPrerequisitesNotSatisfiedException;
	
	public Future<IssueInvoiceResult> issueInvoiceAsynchronously(IssueInvoiceRequest 
		issueInvoiceRequest) throws FinancialPrerequisitesNotSatisfiedException;

	public ProvideInvoiceForEnrollmentResult provideInvoiceForEnrollment(ProvideInvoiceForEnrollmentResult
		provideInvoiceForEnrollmentResult) throws NoInvoiceIssuedForEnrollmentException;

	public Future<ProvideInvoiceForEnrollmentResult> provideInvoiceForEnrollmentAsynchronously
		(ProvideInvoiceForEnrollmentResult provideInvoiceForEnrollmentResult) throws 
		NoInvoiceIssuedForEnrollmentException;
	
}