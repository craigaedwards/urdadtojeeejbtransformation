package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.coursemanagement;

import javax.ejb.Stateless;

@Stateless
public class CourseManagementBean implements CourseManagementLocal, CourseManagementRemote{}