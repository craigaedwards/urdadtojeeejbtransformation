package org.urdadmde.mapping.javaee.example.training.traininginstitution.enrollments;

import java.io.Serializable;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.studyguides.StudyGuide;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.enrollments.proofofenrollments.ProofOfEnrollment;

public class PerformEnrollmentResult implements Serializable
{

	public ProofOfEnrollment getProofOfEnrollment()
	{
		return proofOfEnrollment;
	}

	public void setProofOfEnrollment(ProofOfEnrollment proofOfEnrollment)
	{
		this.proofOfEnrollment = proofOfEnrollment;
	}

	public StudyGuide getStudyGuide()
	{
		return studyGuide;
	}

	public void setStudyGuide(StudyGuide studyGuide)
	{
		this.studyGuide = studyGuide;
	}

	private ProofOfEnrollment proofOfEnrollment;
	private StudyGuide studyGuide;

}