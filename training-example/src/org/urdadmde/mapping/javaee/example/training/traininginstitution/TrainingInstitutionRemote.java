package org.urdadmde.mapping.javaee.example.training.traininginstitution;

import javax.ejb.Remote;

@Remote
public interface TrainingInstitutionRemote extends TrainingInstitution{}