package org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement;

import javax.ejb.Remote;

@Remote
public interface ClientRelationshipManagementRemote extends ClientRelationshipManagement{}