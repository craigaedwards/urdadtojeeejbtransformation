package org.urdadmde.mapping.javaee.example.training.traininginstitution;

import javax.ejb.Stateless;

@Stateless
public class TrainingInstitutionBean implements TrainingInstitutionLocal, TrainingInstitutionRemote{}