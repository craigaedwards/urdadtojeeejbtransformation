package org.urdadmde.mapping.javaee.example.training.traininginstitution.enrollments.presentationmanagement;

import java.io.Serializable;

public class PresentationDetails implements Serializable
{

	public String getCourse()
	{
		return course;
	}

	public void setCourse(String course)
	{
		this.course = course;
	}

	private String course;

}