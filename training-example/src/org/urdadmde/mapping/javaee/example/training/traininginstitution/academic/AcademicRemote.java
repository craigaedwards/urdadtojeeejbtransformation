package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic;

import javax.ejb.Remote;

@Remote
public interface AcademicRemote extends Academic{}