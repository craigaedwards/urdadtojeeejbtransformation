package org.urdadmde.mapping.javaee.example.training.traininginstitution.academic.presentationmanagement;

import java.util.Date;
import org.urdadmde.mapping.javaee.example.training.traininginstitution.finance.Chargeable;

@javax.persistence.Entity
public class Presentation extends Chargeable
{

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public String getLocationIdentifier()
	{
		return locationIdentifier;
	}

	public void setLocationIdentifier(String locationIdentifier)
	{
		this.locationIdentifier = locationIdentifier;
	}

	private String name;
	private Date startDate;
	private String locationIdentifier;

}