package org.urdadmde.mapping.javaee.example.training.traininginstitution.enrollments;

import javax.ejb.Local;

@Local
public interface EnrollmentsLocal extends Enrollments{}