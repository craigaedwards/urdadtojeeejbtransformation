package org.urdadmde.mapping.javaee.example.training.traininginstitution.clientrelationshipmanagement;

import java.io.Serializable;

public class ProvideRegistrationDetailsRequest implements Serializable{

	public String getPersonIdentifier()
	{
		return personIdentifier;
	}

	public void setPersonIdentifier(String personIdentifier)
	{
		this.personIdentifier = personIdentifier;
	}

	private String personIdentifier;

}