package org.urdadmde.mapping.javaee.example.training.client;

import javax.ejb.Remote;

@Remote
public interface ClientRemote extends Client{}