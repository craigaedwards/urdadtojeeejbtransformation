package org.urdadmde.mapping.javaee.utility.javastandardprofilecreator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.emftext.language.java.JavaClasspath;
import org.emftext.language.java.jamoppc.JaMoPPC;

public class JavaStandardProfileCreator extends JaMoPPC
{

	public static void main(String[] args) 
	{
		Options options = new Options();
		options.addOption("h", "help", false, "Print this message");
		options.addOption("c", true, "Classifiers file (One fully qualified classifier per line)");
		options.addOption("j", true, "Jar files directory");
		options.addOption("t", true, "Target directory");
		CommandLineParser commandLineParser = new GnuParser();
		CommandLine commandLine = null;
		HelpFormatter formatter = new HelpFormatter();
		try 
		{
			commandLine = commandLineParser.parse(options, args);
		} 
		catch (ParseException e) {
			e.printStackTrace();
			System.exit(0);
		}
		if ((commandLine.hasOption('h')) || (!commandLine.hasOption("c")) || (!commandLine.hasOption("j")) || 
			(!commandLine.hasOption("t")))
		{
			formatter.printHelp("JavaStandardProfileCreator", options);
			System.exit(0);
		}
		File classifiersFile = new File(commandLine.getOptionValue("c"));
		File jarFilesDirectory = new File(commandLine.getOptionValue("j"));
		File targetDirectory = new File(commandLine.getOptionValue("t"));
		JavaStandardProfileCreator javaStandardProfileCreator = new JavaStandardProfileCreator();
		try 
		{
			javaStandardProfileCreator.create(classifiersFile, jarFilesDirectory, targetDirectory);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	public void create(File classifiersFile, File jarFilesDirectory, File targetDirectory) throws Exception
	{
		if (classifiersFile == null)
		{
			throw new RuntimeException("A classifiers file must be specified");
		}
		if ((!classifiersFile.exists()) || (classifiersFile.isDirectory()))
		{
			throw new RuntimeException("The specified classifiers file does not exist.");
		}
		if (!classifiersFile.getName().endsWith(".txt"))
		{
			throw new RuntimeException("The specified classifiers file should have a .txt file extension.");
		}
		if (jarFilesDirectory == null)
		{
			throw new RuntimeException("A jar files directory must be specified");
		}
		if ((!jarFilesDirectory.exists()) || (!jarFilesDirectory.isDirectory()))
		{
			throw new RuntimeException("The specified jar files directory does not exist.");
		}
		if (targetDirectory == null)
		{
			throw new RuntimeException("A target directory must be specified");
		}
		if ((!targetDirectory.exists()) || (!targetDirectory.isDirectory()))
		{
			throw new RuntimeException("The specified target directory does not exist.");
		}
		if (deleteDirectory(targetDirectory)){
			targetDirectory.mkdirs();	
		}
		System.out.println("Classifiers file '" + classifiersFile.getAbsolutePath() + "'.");
		System.out.println("Jar files directory '" + jarFilesDirectory.getAbsolutePath() + "'.");
		System.out.println("Target directory '" + targetDirectory.getAbsolutePath() + "'.");
		File temporaryClassifiersJavaFile = createTemporaryClassifiersJavaFile(classifiersFile, 
			targetDirectory);
		setUp();
		JavaClasspath javaClasspath = JavaClasspath.get(resourceSet);
		for (File jarFile : listJarFiles(jarFilesDirectory))
		{
			javaClasspath.registerClassifierJar(URI.createFileURI(jarFile.getCanonicalPath()));
		}
		parseResource(temporaryClassifiersJavaFile);
		if (!resolveAllProxies()){
			throw new RuntimeException("Failed to resolve all proxies.");
		}
		createJavaStandardProfileXMIFile(targetDirectory);
		if (!temporaryClassifiersJavaFile.delete())
		{
			throw new RuntimeException("Failed to remove temporary classifiers java file.");
		}
		File javeEcoreFile = new File(targetDirectory.getCanonicalPath() + "/java.ecore");
		if ((javeEcoreFile.exists()) && (!javeEcoreFile.delete()))
		{
			throw new RuntimeException("Failed to remove java.ecore file.");
		}
		File primitiveTypesEcoreFile = new File(targetDirectory.getCanonicalPath() + 
			"/primitive_types.ecore");
		if ((primitiveTypesEcoreFile.exists()) && (!primitiveTypesEcoreFile.delete()))
		{
			throw new RuntimeException("Failed to remove primitive_types.ecore file.");
		}
		System.out.println("Java standard profile file 'javastandardprofile.xmi' has been created.");
	}
	
	private void createJavaStandardProfileXMIFile(File targetDirectory) throws IOException
	{
		File javaStandardProfileXMIFile = new File(targetDirectory.getCanonicalPath() + 
			"/javastandardprofile.xmi");
		Resource xmiResource = resourceSet.createResource(URI.createFileURI(javaStandardProfileXMIFile
			.getCanonicalPath()));
		for (Resource javaResource : new ArrayList<Resource>(resourceSet.getResources())) {
			if (javaResource.getURI().toString().contains("JavaStandardProfileCreator")){
				continue;
			}
			xmiResource.getContents().addAll(javaResource.getContents());
		}
		serializeMetamodel(targetDirectory);
		saveXmiResource(xmiResource);
	}
	
	private boolean deleteDirectory(File directory)
	{
	    if (directory.exists())
	    {
	      for (File file : directory.listFiles())
	      {
	    	 if (file.isDirectory())
	    	 {
	    		 deleteDirectory(file);
	         }
	         else
	         {
	        	 file.delete();
	         }
	      }
		}
	    return directory.delete();
	}
	
	private File createTemporaryClassifiersJavaFile(File classifiersFile, File targetDirectory) throws 
		IOException
	{
		Collection<String> classifiers = listClassifiers(classifiersFile);
		File temporaryClassifiersJavaFile = new File(targetDirectory + "/JavaStandardProfileCreator.java");
		temporaryClassifiersJavaFile.createNewFile();
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(temporaryClassifiersJavaFile));
		try
		{
			for (String classifier : classifiers)
			{
				bufferedWriter.write("import " + classifier + ";");
				bufferedWriter.newLine();
			}
			bufferedWriter.write("public class JavaStandardProfileCreator{}");
		}
		finally
		{
			bufferedWriter.close();
		}
		return temporaryClassifiersJavaFile;
	}
	
	private Collection<String> listClassifiers(File classifiersFile) throws FileNotFoundException, IOException
	{
		Collection<String> classifiers = new LinkedList<String>();
		BufferedReader bufferedReader = new BufferedReader(new FileReader(classifiersFile));
		try
		{
			while(bufferedReader.ready())
			{
				String line = bufferedReader.readLine();
				line = line.trim();
				classifiers.add(line);
			}
		}
		finally
		{
			bufferedReader.close();
		}
		return classifiers;
	}
	
	private Collection<File> listJarFiles(File jarFilesDirectory)
	{
		Collection<File> jarFiles = new LinkedList<File>();
		for (File file : jarFilesDirectory.listFiles())
		{
			if (!file.isDirectory()){
				if (file.getName().toLowerCase().endsWith("jar")){
					jarFiles.add(file);	
				}
			}
			else{
				jarFiles.addAll(listJarFiles(file));
			}
		}
		return jarFiles;
	}
	
}