package org.urdadmde.mapping.javaee.utility.javacodecreator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Collection;
import java.util.LinkedList;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.emftext.language.java.JavaPackage;
import org.emftext.language.java.containers.CompilationUnit;
import org.emftext.language.java.containers.impl.CompilationUnitImpl;
import org.emftext.language.java.resource.JavaSourceOrClassFileResourceFactoryImpl;
import org.emftext.language.primitivetypes.PrimitivetypesPackage;

public class JavaCodeCreator
{

	public static void main(String[] args)
	{
		Options options = new Options();
		options.addOption("h", "help", false, "Print this message");
		options.addOption("j", true, "Java standard profile (Java model file)");
		options.addOption("u", true, "URDAD (Java model file)");
		options.addOption("t", true, "Target directory");
		CommandLineParser commandLineParser = new GnuParser();
		CommandLine commandLine = null;
		HelpFormatter formatter = new HelpFormatter();
		try 
		{
			commandLine = commandLineParser.parse(options, args);
		} 
		catch (ParseException e) {
			e.printStackTrace();
			System.exit(0);
		}
		if ((commandLine.hasOption('h')) || (!commandLine.hasOption("j")) || (!commandLine.hasOption("u")) 
			|| (!commandLine.hasOption("t")))
		{
			formatter.printHelp("JavaCodeCreator", options);
			System.exit(0);
		}
		File javaStandardProfileJavaModelFile = new File(commandLine.getOptionValue("j"));
		File urdadJavaModelFile = new File(commandLine.getOptionValue("u"));
		File targetDirectory = new File(commandLine.getOptionValue("t"));
		JavaCodeCreator javaCodeCreator = new JavaCodeCreator();
		try 
		{
			javaCodeCreator.create(javaStandardProfileJavaModelFile, urdadJavaModelFile, targetDirectory);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	public void create(File javaStandardProfileJavaModelFile, File urdadJavaModelFile, File targetDirectory) 
		throws Exception
		{
		if (javaStandardProfileJavaModelFile == null)
		{
			throw new RuntimeException("A Java standard profile Java model file must be specified");
		}
		if ((!javaStandardProfileJavaModelFile.exists()) || (javaStandardProfileJavaModelFile.isDirectory()))
		{
			throw new RuntimeException("The specified Java standard profile Java model file does not exist.");
		}
		if (!javaStandardProfileJavaModelFile.getName().endsWith(".xmi"))
		{
			throw new RuntimeException("The specified Java standard profile Java model file should have a " +
			".xmi file extension.");
		}
		if (urdadJavaModelFile == null)
		{
			throw new RuntimeException("An URDAD Java model file must be specified.");
		}
		if ((!urdadJavaModelFile.exists()) || (urdadJavaModelFile.isDirectory()))
		{
			throw new RuntimeException("The specified URDAD Java model file does not exist.");
		}
		if (!urdadJavaModelFile.getName().endsWith(".xmi"))
		{
			throw new RuntimeException("The specified URDAD Java model file should have a .xmi file " +
				"extension.");
		}
		if (targetDirectory == null)
		{
			throw new RuntimeException("A target directory must be specified");
		}
		if ((!targetDirectory.exists()) || (!targetDirectory.isDirectory()))
		{
			throw new RuntimeException("The specified target directory does not exist.");
		}
		if (deleteDirectory(targetDirectory)){
			targetDirectory.mkdirs();	
		}
		System.out.println("Java standard profile Java model file '" + javaStandardProfileJavaModelFile
			.getAbsolutePath() + "'.");
		System.out.println("URDAD Java model file '" + urdadJavaModelFile.getAbsolutePath() + "'.");
		System.out.println("Target directory '" + targetDirectory.getAbsolutePath() + "'.");
		replaceText(urdadJavaModelFile, "href=\"" + javaStandardProfileJavaModelFile.getName(), "href=\"" + 
			"file:\\" + javaStandardProfileJavaModelFile.getAbsolutePath());
		/* Register Java resource factory for .java file extension */
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("java", new 
			JavaSourceOrClassFileResourceFactoryImpl());
		/* Register XMI resource factory for .xmi file extension */
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(Resource.Factory.Registry
			.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		URI targetDirectoryUri = URI.createFileURI(targetDirectory.getAbsolutePath());
		URI primitivesEcoreFileURI = targetDirectoryUri.appendSegment("primitives.ecore");
		Resource primitivesEcoreFileResource = resourceSet.createResource(primitivesEcoreFileURI);
		primitivesEcoreFileResource.getContents().add(PrimitivetypesPackage.eINSTANCE);
		URI javaEcoreFileURI = targetDirectoryUri.appendSegment("java.ecore");
		Resource javaEcoreFileResource = resourceSet.createResource(javaEcoreFileURI);
		javaEcoreFileResource.getContents().addAll(JavaPackage.eINSTANCE.getESubpackages());
		/* There is no need to create the 'primitives.ecore' and 'java.ecore' files */ 
		Resource javaStandardProfileJavaModelFileResource = resourceSet.getResource(URI.createFileURI
			(javaStandardProfileJavaModelFile.getAbsolutePath()), true);
		Resource urdadJavaModelFileResource = resourceSet.getResource(URI.createFileURI(urdadJavaModelFile
			.getAbsolutePath()), true);
		Collection<CompilationUnit> compilationUnits = findCompilationUnits(urdadJavaModelFileResource);
		int numberOfJavaCodeFilesCreated = 0;
		for (CompilationUnit compilationUnit : compilationUnits)
		{
			Resource javaCodeFileResource = resourceSet.createResource(URI.createFileURI
				(getJavaCodeFileDirectory(targetDirectory, compilationUnit).getAbsolutePath() + "/" + 
				getJavaCodeFile(compilationUnit)));
			javaCodeFileResource.getContents().add(compilationUnit);
			javaCodeFileResource.save(null);
			numberOfJavaCodeFilesCreated++;
		}
		System.out.println(numberOfJavaCodeFilesCreated + " Java code files have been created.");
	}
	
	private void replaceText(File file, String text, String replacementText) throws Exception
	{
		File temporaryFile = new File(file.getAbsolutePath() + ".tmp");
		BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(temporaryFile));
		String line = null;
		while((line = bufferedReader.readLine()) != null){
			line = line.replace(text, replacementText) + "\r\n";
			bufferedWriter.write(line);
		}
		bufferedWriter.close();
		bufferedReader.close();
		file.delete();
		temporaryFile.renameTo(file.getAbsoluteFile());
	}
		
	private boolean deleteDirectory(File directory)
	{
	    if (directory.exists())
	    {
	      for (File file : directory.listFiles())
	      {
	    	 if (file.isDirectory())
	    	 {
	    		 deleteDirectory(file);
	         }
	         else
	         {
	        	 file.delete();
	         }
	      }
		}
	    return directory.delete();
	}
	
	private Collection<CompilationUnit> findCompilationUnits(Resource javaModelFileResource)
	{
		Collection<CompilationUnit> compilationUnits = new LinkedList<CompilationUnit>();
		TreeIterator<EObject> treeIterator = javaModelFileResource.getAllContents();
		while (treeIterator.hasNext()){
			EObject eObject = (EObject) treeIterator.next();
			if (eObject instanceof CompilationUnitImpl){
				compilationUnits.add((CompilationUnitImpl) eObject);
			}
		}
		return compilationUnits;
	}
	
	private File getJavaCodeFileDirectory(File targetDirectory, CompilationUnit compilationUnit)
	{
		EList<String> namespaces = compilationUnit.getNamespaces();
		String javaCodeFileDirectoryPath = targetDirectory.getAbsolutePath();
		for (String nameSpace : namespaces)
		{
			javaCodeFileDirectoryPath = javaCodeFileDirectoryPath + "/" + nameSpace;
		}
		File javaCodeFileDirectory = new File(javaCodeFileDirectoryPath);
		if (!javaCodeFileDirectory.exists())
		{
			javaCodeFileDirectory.mkdirs();
		}
		return javaCodeFileDirectory;
	}
	
	private String getJavaCodeFile(CompilationUnit compilationUnit)
	{
		String javaCodeFileName = compilationUnit.getName();
		if (javaCodeFileName.endsWith(".java"))
		{
			javaCodeFileName = javaCodeFileName.substring(0, javaCodeFileName.lastIndexOf('.'));
		}
		if (javaCodeFileName.contains("."))
		{
			javaCodeFileName = javaCodeFileName.substring(javaCodeFileName.lastIndexOf('.') 
				+ 1, javaCodeFileName.length());
		}
		return javaCodeFileName + ".java";
	}
	
	private ResourceSet resourceSet = new ResourceSetImpl();
	
}